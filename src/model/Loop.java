package model;

public class Loop {
	
	private String str;
	
	public String loopOne(int area){
		String str = "";
		for(int i = 1 ; i<= area; i++){
			for(int j = 1; j<= area; j++)
				str = str + "*";
			str = str + "\n";
		}
	return str;
	}
	
	public String loopTwo(int area){
		String str = "";
		for(int i = 1 ; i<= area; i++){
			for(int j = 1; j<= area; j++)
				str = str + "*";
			str = str + "\n";
		}
	return str;
	}
	
	public String loopThree(int area){
		String str = "";
		for(int i = 1; i <= area; i++){
			for (int j = 1; j <= i; j++)
				str = str + "*";
			str = str + "\n";
		}
	return str;
	}
	
	public String loopFour(int area){
		String str = "";
		for(int i =1; i <= area ; i++){
			for(int j = 1; j<= area; j++)
				if (j%2 == 0){
					str = str + "*";
				}
				else {
					str = str +"-";
				}
		str = str + "\n";
		}
		return str;
	}
	
	public String loopFive(int area){
		String str = "";
		for(int i = 1; i <= area; i++){
			for(int j =1; j<= area; j++){
				if((i+j)%2 == 0){
					str = str + "*";
				}
				else {
					str = str + " ";
				}
			}
		str = str + "\n";
		}
		return str;
	}
}
