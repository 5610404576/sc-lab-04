package view;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class LoopGui extends JFrame {
	JFrame frame;
	JLabel label1,label2;
	JPanel panel1,panel2,panel3;
	JTextArea result;
	JTextField inputarea;
	JButton enterbut,endbut ;
	String str="";
	public JComboBox looptype;
	
	public LoopGui(){
		createFrame();
	}
	
	public void createFrame(){
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		
		result = new JTextArea(22,25);
		
		inputarea = new JTextField(5);
		
		enterbut = new JButton("RUN");
		enterbut.setBounds(0,0,120,50);
		
		endbut = new JButton("End");
		endbut.setBounds(0,0,120,50);
		
		looptype = new JComboBox();
		looptype.setBounds(0,0,120,50);
		looptype.addItem("Loop 1");
		looptype.addItem("Loop 2");
		looptype.addItem("Loop 3");
		looptype.addItem("Loop 4");
		looptype.addItem("Loop 5");
		looptype.setSelectedIndex(0);
		panel2.add(looptype);
		
		
		panel1.add(result);
		
		panel2.add(inputarea);
		panel2.add(enterbut);
		panel2.add(endbut);
		
		add(panel1);
		add(panel2);
		 
	}
	public void setResult(String str){
		this.str = str;
		result.setText(str);
		}
	
	public String getInput1(){
		 return inputarea.getText();
	   }
	
	public void setListener(ActionListener list){
		 endbut.addActionListener(list);
	   }
	
	public void setListener2(ActionListener list){
		 enterbut.addActionListener(list);
	   }
}
