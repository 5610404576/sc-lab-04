package controller;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import view.LoopGui;
import model.Loop;

public class TestCase {
	private LoopGui frame;
	private Loop model;
	private ActionListener list;
	
	public static void main(String[] args) {
		new TestCase();
	}
	
	public TestCase(){
		frame = new LoopGui();
		model = new Loop();
		
		frame.pack();
        frame.setVisible(true);
        frame.setSize(600,400);
        frame.getContentPane().setLayout(new GridLayout(0, 2));
        frame.setListener(list);

        frame.setListener(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);		   	               	
	         }            
		});
        
        frame.setListener2(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	String n ="Loop 1";
	        	if(n == frame.looptype.getSelectedItem()){
	        		int inputarea = Integer.parseInt(frame.getInput1());
	        		frame.setResult(model.loopOne(inputarea));
	        	}
	        	
	        	String i ="Loop 2";
	        	if(i == frame.looptype.getSelectedItem()){
	        		int inputarea = Integer.parseInt(frame.getInput1());
	        		frame.setResult(model.loopTwo(inputarea));
	        	}
	        	
	        	String j ="Loop 3";
	        	if(j == frame.looptype.getSelectedItem()){
	        		int inputarea = Integer.parseInt(frame.getInput1());
	        		frame.setResult(model.loopThree(inputarea));
	        	}
	        	String k = "Loop 4";
	        	if(k == frame.looptype.getSelectedItem()){
	        		int inputarea = Integer.parseInt(frame.getInput1());
	        		frame.setResult(model.loopFour(inputarea));
	        	}
	        	
	        	String l = "Loop 5";
	        	if(l == frame.looptype.getSelectedItem()){
	        		int inputarea = Integer.parseInt(frame.getInput1());
	        		frame.setResult(model.loopFive(inputarea));
	        	}
	         }            
		});
	}
}
